classdef FemModel
    %FemModel Holds data for a finite element model
    %   This class holds data needed to operato of a FEM model
    
    properties
        edofs
        nodes
    end
    
    properties(SetAccess=private)
        dofsPerNode
        dim
    end
    
    methods
        function obj = FemModel(dim, dofsPerNode)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            arguments
                dim {mustBePositive} = 3
                dofsPerNode {mustBePositive} = 3
            end
            obj.dim = dim;
            obj.dofsPerNode = dofsPerNode;
            obj.nodes = zeros(0, dim);
            obj.edofs = zeros(0, 0);
        end
        
        function n=nodesCount(obj)
            n = size(obj.nodes, 1);
        end
        
        function n=elemsCount(obj)
            n = size(obj.edofs, 1);
        end
        
        function outputArg = method1(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
    end
end

