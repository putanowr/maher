% Run all tests for maher project
function result = mh_run_tests(~)
    import matlab.unittest.TestSuite

    suiteClass = TestSuite.fromClass(?TestFemModel);
    result = run(suiteClass);
end