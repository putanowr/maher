classdef TestFemModel < matlab.unittest.TestCase
    properties
       empty_model
    end
    
    methods(TestMethodSetup)
        function createModel(testCase)
            testCase.empty_model = mh.FemModel;
        end
    end
    
    methods(Test)
        function emptyModel(testCase)
            testCase.verifyEqual(testCase.empty_model.elemsCount(), 0, ...
                'Empty model should not contain elements') 
        end
    end
end